<?php include('header.php');?>

<section id="wrapper" class="login-register login-sidebar" style="background-image:url(assets/images/background/background.jpg);">
    <div class="login-box card">
        <div class="card-body">
            
            <form class="form-horizontal form-material" id="loginform" action="regsave.php" method="POST">

                <a href="javascript:void(0)" class="text-center db"><img src="assets/images/logo.png" alt="Home" style="height:20%; width: 40%;"/><br/><img src="assets/images/logo-text1.png" alt="Home" /></a>
                <h3 class="box-title m-t-40 m-b-0">Register Now</h3><small>Create your account and enjoy</small>
                <div class="form-group m-t-20">
                    <?php 
                    if(isset($msg)){
                        echo '<label class="text-danger;">'. $msg .'</label>';
                    }
                    ?>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" placeholder="Name" name="name" required="">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" placeholder="Username" name="username" required="">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" placeholder="Password" name="password" required="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" placeholder="Confirm Password" name="confirmpw" required="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary p-t-0">
                            <input id="checkbox-signup" type="checkbox" required="">
                            <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" name="register" type="submit">Sign Up</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Already have an account? <a href="index.php" class="text-info m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>


<?php include('footer.php');?>