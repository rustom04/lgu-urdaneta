<?php include('header.php');?>
<?php include('navigation.php'); ?>
<?php include('menu.php');?>

<style type="text/css">
    .input-group-sm>.form-control {
        font-size:12px;
    }
    
    .form-control {
        font-size:12px;
    }
</style>



<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">User Management</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">User Management</li>
                <li class="breadcrumb-item active">Add User</li>
            </ol>
        </div>
    </div>

   	<div class="container-fluid">
        <form action="save_user.php">
            <?php include('UserCreate.php');?>
        </form>
    </div>

</div>
<?php include('footer.php');?>



