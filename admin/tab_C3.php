<h6>Step 3</h6>
<section>
    <legend>
        <h1>(Page 3)</h1>
    </legend>
    <div class="row">
        <!-- Row for Eligibility -->
        <div class="row">
            <div class="col-lg-12">
                <h3>VI. VOLUNTARY WORK OR INVOLVEMENT IN CIVI/ NON-GOVERMENT/ PEOPLE/ VOLUNTARY ORGANIZATIONS</h3>
            </div>
            <div class="col-lg-12" style="font-size: 12px">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td rowspan="2" width="0">29. NAME & ADDRESS OF ORGANIZATION <br><small>(Write in full)</small></td>
                            <td colspan="2" width="0">INCLUSIVE DATES <small>(mm/dd/yyyy)</small></td>
                            <td rowspan="2" width="0">NUMBER OF HOURS</td>
                            <td rowspan="2" width="0">POSITION/ NATURE OF WORK</td>
                            <td rowspan="2" width="0">
                                <button type="button" id="BtnVoluntary" class="btn btn-info">
                                    <i class="fa fa-plus-square"></i>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td width="0">from</td>
                            <td width="0">to</td>
                        </tr>
                    </thead>
                    <tbody class="voluntary">
                        <tr>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Row for Work Exp -->
        <div class="row">
            <div class="col-lg-12">
                <h3>V. WORK EXPERIENCE</h3>
            </div>
            <div class="col-lg-12" style="font-size: 12px">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td colspan="2" width="0">28. INCLUSIVE DATES <small>(mm/dd/yyyy)</small></td>
                            <td rowspan="2" width="0">POSITION <br><small>(Write in full/ Do not abbreviate)</small></td>
                            <td rowspan="2" width="0">DEPARTMENT/ AGENCY/ OFFICE/ COMPANY <br><small>(Write in full/ Do not abbreviate)</small></td>
                            <td rowspan="2" width="0">MONTHLY SALARY</td>
                            <td rowspan="2" width="0">SALARY/ JOB/ PAY GRADE <br><small>(if applicable)</small> & STEP <small>(format "00-0")</small> / INCREMENT</td>
                            <td rowspan="2" width="0">STATUS OF APPOINTMENT</td>
                            <td rowspan="2" width="0">GOV'T SERVICE (Y/N)</td>
                            <td rowspan="2" width="0">
                                <button type="button" id="addWorkExp" class="btn btn-info">
                                    <i class="fa fa-plus-square"></i>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td width="0">from</td>
                            <td width="0">to</td>
                        </tr>
                    </thead>
                    <tbody class="workexp">
                        <tr>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <input type="text" id="" class="form-control" name="child_name">
                            </td>
                            <td>
                                <button id="minus" class="remove btn btn-danger"><i class="fa fa-minus-square"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</section>