<?php include('header.php');?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->


    <?php
        $username = @$_POST['username'];
        $password = @$_POST['password'];
        $login = @$_POST['login'];

        if ($login) {
        if ($username == "" || $password == "") {
          ?><script type="text/javascript">alert("Username and Password is incorrect!")</script><?php
        }else{
        $login_query = mysqli_query($con,"SELECT * FROM tbl_user WHERE username = '$username' and password = md5('$password') ")or die(mysqli_error());
        $row = mysqli_num_rows($login_query);
        $result = mysqli_fetch_array($login_query);


        if ($row >= 1) {

            if ($result['user_type'] == "admin") {
                  @$_SESSION['admin'] = $result['id'];
                    //display session into applicant page
                    $_SESSION['id'] = $result['id'];
                    $_SESSION['user_type'] = $result['user_type'];
                    $_SESSION['fullname'] = $result['fullname'];
                    $_SESSION['position'] = $result['position'];


            if ($row > 0){
                  session_start();

                  header("location: admin/dashboard.php");

              }

            }else{

            ?>
            <script type="text/javascript">
              alert("Invalid Username or Password! Please try again.");
              window.location.href='index.php'
            </script>
            <?php

            }
        }else{

        ?>
        <script>
            $(window).load(function(){
                $('#loginFailed').modal('show');
                setTimeout(function (){
                $('#loginFailed').hide().modal('show');
                });
            });
        </script>
        <?php
        }
        }
        }
    ?>


    <section id="wrapper" class="login-register login-sidebar" style="background-image:url(assets/images/background/background.jpg);">
        <div class="login-box card">
            <div class="card-body">

                <form class="form-horizontal form-material" id="loginform" method="POST">
                    <a href="javascript:void(0)" class="text-center db"><img src="assets/images/logo.png" alt="Home" style="height:20%; width: 40%;"/><br/><img src="assets/images/logo-text1.png" alt="Home" /></a>

                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Username" name="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" placeholder="Password" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>
                            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <!-- <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" name="login" type="submit">Log In</button> -->

                            <input type="submit" name="login" class="btn btn-info btn-lg btn-block " value="Log In">
                        </div>
                    </div>

                    <?php 
                    // if(!empty($msg)){
                    //     if(isset($msg)){
                    //     echo '<div class="col-sm-12 text-center"><small><label class="text-danger alert alert-danger">'. $msg .'</label></small></div>';
                    //     }
                    // }
                    ?>
                    
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Don't have an account? <a href="signup.php" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="index.html">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

<?php include('footer.php');?>